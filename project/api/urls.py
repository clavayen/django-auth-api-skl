from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from . import views


api_patterns = [
    path('admin/', admin.site.urls),
    path('status/', include('watchman.urls')),
    path('public/', include('public_app.urls')),
]

if settings.APP_ENV == 'local':
    api_patterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

shared_endpoints = [
    re_path(r'^oauth/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    re_path(r'^restricted/(?P<test_id>[a-zA-Z0-9-_]+)/?$', views.TestEndpointView.as_view(), name='update'),
]

api_patterns += shared_endpoints

urlpatterns = api_patterns
