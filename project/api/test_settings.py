"""
api.test_settings
"""

from .settings import *  # NOQA


CACHES['default'] = {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
}

# To disable caching of singleton configurations during tests
SOLO_CACHE = None
