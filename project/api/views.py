from rest_framework.views import APIView
from rest_framework.response import Response
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication
from rest_framework import status as response_status


class TestEndpointView(APIView):
    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenHasReadWriteScope]

    def get(self, request, test_id):
        response = {
            "message": "success from protected endpoint"
        }
        return Response(data=response, status=response_status.HTTP_200_OK)

    def post(self, request, test_id):
        response = {
            "id": test_id,
            "message": "Success response from protected endpoint"
        }
        return Response(data=response, status=response_status.HTTP_200_OK)
