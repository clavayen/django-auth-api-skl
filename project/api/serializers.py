from rest_framework import serializers

class StoreSerializer(serializers.Serializer):
    id = serializers.CharField()
    pump_number = serializers.CharField()
    pos_version = serializers.CharField()
    pos_sequence_id = serializers.CharField()
    loyalty_offline_flag = serializers.BooleanField(default= False)
    time_zone = serializers.CharField()

class BrandSerializer(serializers.Serializer):
    name = serializers.CharField(required=False)
    sub_name = serializers.CharField(allow_null=True, required=False)
    version = serializers.CharField(required=False)


class TransactionDataSerializer(serializers.Serializer):
    cashier_id = serializers.CharField()
    till_id = serializers.CharField(required=False)
    pos_transaction_id = serializers.CharField()
    outside_sales_flag = serializers.BooleanField(default= True)


class PromptsSerializer(serializers.Serializer):
    id = serializers.CharField()
    response = serializers.CharField()


class ProgramDataSerializer(serializers.Serializer):
    stateful_echo = serializers.CharField()


class AdapterRequestSerializer(serializers.Serializer):
    member_id = serializers.CharField()
    entry_method = serializers.CharField()
    channel = serializers.CharField()
    transaction_id = serializers.CharField(required=False)
    program = ProgramDataSerializer(required=False)
    store = StoreSerializer()
    brand = BrandSerializer()
    transaction = TransactionDataSerializer(required=False)
    prompt = PromptsSerializer(required=False)


class PromptsDataSerializer(serializers.Serializer):
    id = serializers.CharField(allow_null=True)
    type = serializers.CharField(allow_null=True)
    text_short = serializers.CharField(allow_null=True)
    text_long = serializers.CharField(allow_null=True)
    multi_line_text = serializers.ListField(allow_empty=True)
    device = serializers.CharField()
    timed_out = serializers.IntegerField()
    min_length = serializers.CharField(allow_null=True, required=False)
    max_length = serializers.CharField(allow_null=True, required=False)
    user_input = serializers.BooleanField(required=False)
    masked_input = serializers.BooleanField(required=False)


class RewardsDataSerializer(serializers.Serializer):
    id =  serializers.CharField(allow_null=True)
    text_short = serializers.CharField(allow_null=True)
    text_long = serializers.CharField(allow_null=True)
    device = serializers.CharField(allow_null=True)
    timed_out = serializers.IntegerField()
    discount_method = serializers.CharField(allow_null=True)
    discount_value = serializers.FloatField(required=False)
    # fuel_data = serializers.ListField()
    # options = serializers.ListField()


class AdapterResponseSerializer(serializers.Serializer):
    """
    {
        "store_id": "33126",
        "pump_number": 1,
        "transaction_id": "uuid",
        "pos_sequence_id": "123456",
        "loyalty_ID_valid_flag":true,
        "program": {
            "stateful_echo": "<![CDATA[<StatefulEcho xmlns=\"http://www.comarch.com/CPPM/StatefulEcho\"><TransactionContext><OfferGiven>true</OfferGiven><PointsLocked>true</PointsLocked><CustomerIdentification><Alias>+12145005518</Alias><AliasType>3</AliasType></CustomerIdentification></TransactionContext></StatefulEcho>]]>"
        },
        "prompts": [
            {
                "id": "PIN_AUTH",
                "type": "Numeric",
                "text_short": "Enter Code",
                "text_long": "Enter Code",
                "multi_line_text": [],
                "device": "posCashierDisplay",
                "timed_out": 30,
                "min_length": "4",
                "max_length": "4",
                "masked_input": true,
                "user_input": true,
            }
        ],
        "rewards": [
            {
                "id": "emrDollarOff",
                "text_short": "Enter Code",
                "text_long": "Enter Code",
                "device": "pump" or "pos",
                "timed_out": 30,
                "discount_method": "Dollar off" or "cpg off",
                "discount_value": "50.00",
                "fuel_data":[], # Future value.
            }
        ]
    }
    """

    store_id = serializers.CharField()
    pump_number = serializers.IntegerField()
    transaction_id = serializers.CharField()
    pos_sequence_id = serializers.CharField()
    loyalty_ID_valid_flag = serializers.BooleanField()
    program = ProgramDataSerializer()
    prompts = PromptsDataSerializer(many=True, required=False)
    rewards = RewardsDataSerializer(many=True, required=False)


class TenderInfoSerializer(serializers.Serializer):
    code = serializers.CharField()
    sub_code = serializers.CharField()
    card_id = serializers.CharField()
    amount = serializers.FloatField()
    currency = serializers.CharField()


class TaxDetailDataSerializer(serializers.Serializer):
    tax_level_id  = serializers.CharField(required=False)
    tax_collected_amount = serializers.FloatField()


class TransactionDetailsDataSerializer(serializers.Serializer):
    transaction_line_status = serializers.CharField()
    fuel_prepay_flag = serializers.BooleanField()
    grade_id = serializers.CharField()
    pump_number = serializers.IntegerField()
    payment_system_product_code = serializers.CharField()
    service_level_code = serializers.CharField()
    grade_description = serializers.CharField()
    currency = serializers.CharField()
    sales_uom = serializers.CharField()
    discounted_price = serializers.FloatField()
    regular_price = serializers.FloatField()
    volume = serializers.FloatField()
    unit = serializers.CharField()
    total_amount = serializers.FloatField()
    tax = TaxDetailDataSerializer(required=False)
    tender_info = TenderInfoSerializer(many=True)


class FinalizeTransactionRequestSerializer(AdapterRequestSerializer):
    transaction_details = TransactionDetailsDataSerializer()


class RecieptSerializer(serializers.Serializer):
    messages = serializers.ListField()


class FinalizeTransactionResponseSerializer(serializers.Serializer):
    store_id = serializers.CharField()
    pump_number = serializers.IntegerField()
    transaction_id = serializers.CharField()
    pos_sequence_id = serializers.CharField()
    receipt = RecieptSerializer()

class StoreDataSerializer(serializers.Serializer):
    id = serializers.CharField()
    merchant_id = serializers.CharField()
    pump_number = serializers.CharField(allow_null=True)
    name = serializers.CharField(required=False)
    version = serializers.JSONField(required=False)
    sequence_id = serializers.CharField(required=False, allow_null=True)
    loyalty_offline_flag = serializers.BooleanField(default=False)
    time_zone = serializers.CharField(required=False)

class SiteDetailSerializer(serializers.Serializer):
    mobile_active = serializers.BooleanField(default=False)
    partial_auth_allowed = serializers.BooleanField(default=False)

class SiteTransactionDataSerializer(serializers.Serializer):
    workstation_id = serializers.CharField()
    pos_transaction_id = serializers.CharField()

class GradesSerializer(serializers.Serializer):
    abbr = serializers.CharField()
    name = serializers.CharField()
    grade_id = serializers.CharField()
    original_street_price = serializers.FloatField()
    final_price = serializers.FloatField()
    pos_code = serializers.CharField(required= False)
    pos_code_modifier = serializers.CharField(allow_blank=True, required= False)
    product_code = serializers.CharField(required= False)
    pump_number = serializers.CharField()
    unit_measure = serializers.CharField()
    price_tier = serializers.CharField()
    service_level = serializers.CharField()
    evaluate_only = serializers.BooleanField(default=True, required= False)
    is_pump_product = serializers.BooleanField(default=True, required= False)
    item_id = serializers.CharField()
    discount_breakdown = serializers.ListField(allow_null=True)

class CarwashPackageSerializer(serializers.Serializer):
    pos_code = serializers.CharField()
    pos_code_modifier = serializers.CharField(allow_blank=True, required=False)
    product_code = serializers.CharField()
    original_amount = serializers.JSONField()
    adjusted_amount = serializers.JSONField(required=False)
    description = serializers.CharField()

class FuelDataSerializer(serializers.Serializer):
    pump_timeout = serializers.IntegerField()
    status = serializers.CharField()
    grades = GradesSerializer(many=True, required=False)
    last_updated = serializers.CharField()

class CarWashDataSerializer(serializers.Serializer):
    status = serializers.CharField()
    packages = CarwashPackageSerializer(many=True, required=False)

class LoyaltyDetailserializer(serializers.Serializer):
    program_id = serializers.CharField()
    program_name = serializers.CharField()
    status = serializers.CharField()

class TransactionDetailSerializer(serializers.Serializer):
    pump_timeout = serializers.IntegerField()
    status = serializers.CharField()

class SiteDataRequestSerializer(serializers.Serializer): #sitedata #storedata
    status = serializers.CharField()
    fuelTransactionId = serializers.CharField()
    loyalty_id = serializers.CharField(allow_null=True)
    time_stamp = serializers.CharField()
    host_mppa_identifier = serializers.CharField()
    currency = serializers.CharField()
    message_type = serializers.CharField()
    store = StoreDataSerializer()
    brand = BrandSerializer()
    site_details = SiteDetailSerializer()
    fuel_data = FuelDataSerializer(required=False)
    carwash_data= CarWashDataSerializer(required=False)
    loyalty_details = LoyaltyDetailserializer(many=True, required=False)

class SiteDataResponseSerializer(serializers.Serializer):
    status = serializers.CharField()
    loyalty_id = serializers.CharField(allow_null=True)
    fuelTransactionId = serializers.CharField()
    response_details = serializers.JSONField()

class ResponseDetailsSerializer(serializers.Serializer):
    overall_result = serializers.CharField()
    response_code = serializers.CharField()
    message_code = serializers.CharField()

class TransactionUpdateRequestSerializer(serializers.Serializer):
    """
    {
        "status": "Pump Reserved",
        "message_type": "MobilePumpReserveResponse", 
        "loyalty_id": "1758773300375795961",
        "fuelTransactionId": "d104f653-0560-472e-811a-9e86f22050e8",
        "time_stamp": "2023-01-25T17:15:55.792Z",
        "host_mppa_identifier": "P97",
        "currency": "USD",
        "store": {
            "merchant_id": "1234-5678",
            "id": "10202",
            "pump_number": 1,
            "name": "TEST STORE", optional
            "version": {}, optional
            "sequence_id": null, optional
            "loyalty_offline_flag": false, optional 
            "time_zone": "America/Chicago", or null
        },
        "response_details": {
            "overall_result": "Success",
            "response_code": "00000",
            "message_code": "Success"
        },
        "fuel_data": {},
        "brand":{},
        "payment_data": {},
        "transaction_data":{}
    }
    """
    status = serializers.CharField()
    message_type = serializers.CharField()
    loyalty_id = serializers.CharField(required=False, 
                                       allow_blank=True,
                                       allow_null=True)
    fuelTransactionId = serializers.CharField()
    time_stamp = serializers.CharField()
    host_mppa_identifier = serializers.CharField()
    currency = serializers.CharField()
    store = StoreDataSerializer()
    response_details = ResponseDetailsSerializer()
    callback_url = serializers.JSONField(default={}, required=False)
    fuel_data = serializers.JSONField(default={})
    payment_data = serializers.JSONField(default={})
    brand = serializers.JSONField(default={})
    transaction_data = serializers.JSONField(default={})

class TransactionUpdateResponseSerializer(serializers.Serializer):
    """
    {
        "status": "Pump Reserved",
        "fuelTransactionId": "F3A9EB8A-07BD-40BD-81E2-9F301A9B1A58",
        "loyalty_id": null,
        "response_details": {
            "overall_result": "Success",
            "response_code": "00000",
            "message_code": "Success"
        }
    }
    """
    status = serializers.CharField()
    fuelTransactionId = serializers.CharField()
    loyalty_id = serializers.CharField(required=False, 
                                       allow_blank=True,
                                       allow_null=True)
    response_details = ResponseDetailsSerializer()
