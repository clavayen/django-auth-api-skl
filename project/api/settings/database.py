import json
import os


APP_ENV = os.environ.get('APP_ENV', 'local')


db_secrets = json.loads(
    os.environ.get('SECRETS_DATABASE', '{}')
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': os.environ.get('DB_HOST', db_secrets.get('host', 'db')),
        'PORT': os.environ.get('DB_PORT', '5432'),
        'USER': os.environ.get('DB_USER', db_secrets.get('username', 'local')),
        'PASSWORD': os.environ.get('DB_PASSWORD', db_secrets.get('password', 'api_password')),
        'NAME': os.environ.get('DB_NAME', 'api_db'),
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
