"""
Stores API
"""
from __future__ import absolute_import, unicode_literals

# This version number is used across a variety of places during the build and deploy process.
__version__ = '0.0.1'

__title__ = 'stores-api'

__author__ = '7-Eleven'
