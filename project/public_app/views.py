from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status as response_status
from rest_framework.permissions import AllowAny

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication


class TestOneView(APIView):
    # authentication_classes = [OAuth2Authentication]
    # permission_classes = [TokenHasReadWriteScope]
    permission_classes = [AllowAny]

    def post(self, request):
        response = {
            "message": "Response from public endpoint TestOne",
        }
        return Response(data=response, status=response_status.HTTP_200_OK)


class TestTwoView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        response = {
            "message": "Response from public endpoint TestTwo",
        }
        return Response(data=response, status=response_status.HTTP_200_OK)


class TestThreeView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        response = {
            "message": "Response from public endpoint TestThree",
        }
        return Response(data=response, status=response_status.HTTP_200_OK)
