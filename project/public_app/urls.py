from django.urls import re_path
from . import views

app_name = 'dummy'
urlpatterns = [
    re_path(r'^path1/?$', views.TestOneView.as_view(), name='testpath-path-1'),
    re_path(r'^path2/?$', views.TestTwoView.as_view(), name='testpath-path-2'),
    re_path(r'^path3/?$', views.TestThreeView.as_view(), name='testpath-path-3'),
]
