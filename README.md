Django API REST + Docker
=======

A Django microservice that provides API's public and private with oAuth.

- CDK Deployments
- `docker` and `docker-compose` setup
- A .gitignore file for common things to ignore

## Pre-requisites
1.  **Python 3.8**
2. **Docker & Docker Compose**


## Getting Started
Build Docker image
```shell script
docker compose build
```

After building the image you should startup the *django-test-api-server-api* container
```shell script
docker compose up -d
```

See if the django test server container is running by executing next command. You should be able to see two containers wunning: Api Server and Postgre DB
1. django-test-api-server-api-1
2. django-test-api-server-db-1
```shell script
docker ps
```

Once they are running you should be able to execute the Application fixture to get the oAuth credentials: *client_id* & *client_secret*

Load your local environment with Application fixtures:
```shell script
docker compose exec api bash -c "python manage.py loaddata api/fixtures/admin.json"
```

Then you can use the Postman's collection to make request to get the token and test the endpoints

In case you need to enter to the Django Admin console, you need to create a Super User by executing next command:
```shell script
docker compose exec api bash -c "python manage.py createsuperuser --username test --email test@email.com"
```

Once you entered the password, you can navigate to the `localhost:8000/admin` to login in the Django Admin UI

You can use next credentials to get OAuth token:
```bash
cliend_id: H86sAv8kH2j2wbMhbFw3sRzC4FRQz8X0BJxjGjWA
cliend_secret: bFHCND0OGN3CWDbWy1wDyOQmPteh5yJjxcYWxg02SGvthiAmsjeyrrYdhdpXPmqrY5FSXzFDZLHH6ROKKPzoGagB2DDQ4dxpfcr4pPOHI8bIGiIEisgXt6UMH6HzOWc6
grant_type": "client_credentials
```