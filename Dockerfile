# FROM ubuntu:latest
FROM python:3.8.10

RUN apt-get update
# RUN apt-get install -y libgdal-dev ## Geospatial library
# RUN rm -rf /var/lib/apt/lists/*    ## WHY????
# RUN apt-get clean && apt-get autoremove
RUN apt-get install software-properties-common -y
# RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update

# ENV DEBIAN_FRONTEND=noninteractive
# RUN apt-get install python3.8 -y
# RUN apt remove python-pip -y && apt-get install python3-pip -y
# RUN pip install --upgrade pip
# RUN pip install pipenv

# COPY requirements.txt requirements.txt
# RUN pip install -r requirements.txt

# COPY docker-entrypoint.sh /srv/docker-entrypoint.sh
# COPY project .

WORKDIR /app

# The following ENV vars are only used at runtime so you can keep them at the bottom of this file
ENV APP_MODULE 'api.wsgi:application'
ENV GUNICORN_CONFIG '/app/gunicorn.py'

# Set Bash shell as entrypoint of the container
ENTRYPOINT ["/bin/bash"]
