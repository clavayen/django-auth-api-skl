#!/bin/bash

bash $STARTUP_SCRIPT || true;

if [ "$1" == "startapp" ]; then
  pip install ipython && \
  pip install -r /requirements.txt && \
  python manage.py migrate --noinput && \
  python manage.py collectstatic --noinput && \
  python manage.py loaddata api/fixtures/admin.json && \
  gunicorn $APP_MODULE --config=$GUNICORN_CONFIG

else
   echo "No service declared for the docker-entrypoint.sh to use."
   exit 1
fi
